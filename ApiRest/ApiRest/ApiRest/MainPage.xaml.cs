﻿using ApiRest.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ApiRest
{
	public partial class MainPage : ContentPage
	{
        private const string Url = "https://jonmidpruebanode.herokuapp.com/users";
        private readonly HttpClient client = new HttpClient();
        private ObservableCollection<Users> _user;

        public MainPage()
		{
			InitializeComponent();
		}

        async public void ListData()
        {
            string content = await client.GetStringAsync(Url);
            List<Users> user = JsonConvert.DeserializeObject<List<Users>>(content);

            _user = new ObservableCollection<Users>(user);
            listViewUsers.ItemsSource = _user;
        }

        async public void CreateData()
        {
            Users users = new Users()
            {
                Name = "Carlos"
            };

            var json = JsonConvert.SerializeObject(users);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await client.PostAsync(Url, content);
        }

        public void ClickListData(object sender, EventArgs e)
        {
            ListData();
        }

        public void ClickCreateData(object sender, EventArgs e)
        {
            CreateData();
        }

        // Cada ves que se ejecute la pantalla
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ListData();
        }
    }
}
